#!/bin/sh

# Update latest git tag.
echo "Ascertaining the latest Haiku hrev tag..."
cd /boot/home/src/haiku
tag=$(git tag --list hrev[0-9]* --sort=v:refname|tail --lines=1)
echo "Found most recent hrev tag: $tag"

# x86_gcc2 hybrid
cd /boot/home/src/haiku/generated.x86gcc2

# Check existence of previous build image
if [ -f haiku-nightly_arfonzo.iso ]; then
	echo "Error: haiku-nightly_arfonzo.iso already exists."
	exit
fi

# Build 'arfonzo-cd'
jam -q @arfonzo-cd

# Rename based on hrev.
if [ -f haiku-nightly_arfonzo.iso ]; then
	mv haiku-nightly_arfonzo.iso haiku-nightly_arfonzo-$tag.iso
fi
